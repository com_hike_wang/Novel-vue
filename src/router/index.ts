import Vue from 'vue';
import Router from 'vue-router';
import Login from '../views/Login.vue';
import Layout from '@/layout/Layout.vue';

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return (originalPush.call(this, location) as any).catch(err => err);
};
Vue.use(Router);
// 公共路由
export const constantRoutes = [
    {
        path: '',
        component: Layout,
        redirect: 'home',
        visible: true,
        children: [
            {
                path: 'home',
                component: () => import('@/views/Home.vue'),
                name: '首页',
                meta: {title: '首页', icon: 'el-icon-monitor', cache: true, affix: true},
            },
        ],
    },
    {
        path: '/login',
        name: '用户登录',
        visible: true,
        component: Login,
    },
    {
        path: '/user',
        name: '个人中心',
        visible: true,
        component: Layout,
        children: [
            {
                path: 'profile',
                visible: true,
                name: '个人信息',
                component: () => import('@/views/Profile.vue'),
                meta: {
                    title: '个人信息',
                    icon: '',
                    cache: false
                },
            },
        ],
    },
    {
        path: '/redirect*',
        component: Layout,
        visible: true,
        children: [
            {
                path: '/redirect/:path*',
                visible: true,
                meta: {title: 'redirect', cache: false},
                component: () => import('@/views/Redirect.vue')
            }
        ]
    },
    {
        path: '/404',
        name: '404',
        visible: true,
        meta: {title: '404', cache: false},
        component: () => import('@/views/404.vue'),
    },
];
const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: constantRoutes,
});


export default router;
