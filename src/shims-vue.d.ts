declare module '*.vue' {
    import Vue from 'vue';
    export default Vue;

    export interface MyDialog {
        dialogFormVisible: boolean;
        title: string;
    }

    export interface Result {
        code: number;
        result: boolean;
        msg: string;
        data: any;
    }

    export interface Params {
        ksy: string;
    }

    export interface TableDataInfo {
        total: number;
        code: number;
        rows: any[];
    }

    export interface BindingResultObject {
        field: string,
        message: string
    }
}
