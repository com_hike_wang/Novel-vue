const DEBUG = process.env.NODE_ENV !== 'production';

/**
 * 错误打印提示
 * @param args
 */
export const logError = (...args: any[]) => {
    if (DEBUG) {
        console.error(...args);
    } else {
        console.log(...args);
    }
};

/**
 * 警告打印提示
 * @param args
 */
export const logWarn = (...args: any[]) => {
    if (DEBUG) {
        console.log(...args);
    }
};

/**
 * 一般打印提示
 * @param args
 */
export const log = (...args: any[]) => {
    if (DEBUG) {
        console.log(...args);
    }
};

export default log;
