import Vue from 'vue';
import store from '@/store';

Vue.directive('permission', {
    bind(el, binding) {
        if (!Vue.prototype.$permission(binding.value)) {
            el.style.display = 'none';
        }
    },
});
// 权限检查方法
Vue.prototype.$permission = function (value: any) {
    let isExist = false;
    const permission = store.getters.permission;
    // 权限列表
    for (let i = 0; i < permission.length; i++) {
        if (permission[i].indexOf('*:*:*') > -1) {
            isExist = true;
            break;
        }
        if (permission[i].indexOf(value) > -1) {
            isExist = true;
            break;
        }
    }
    return isExist;
};
