import http from '@/utils/http';

/**
 * 登录方法
 * @param userName 用户名
 * @param password 用户密码
 * @param code 验证码
 * @param key 验证码id
 * @param rememberMe 记住登录
 */
export function login(userName: string, password: string, code: string, key: string, rememberMe: boolean): any {
    const data = {
        userName,
        password,
        rememberMe,
        code,
        key,
    };
    return http({
        url: '/login',
        method: 'post',
        params: data,
    });
}


/**
 * 用户登出
 * @returns {*|Promise<any>}
 */
export function logout(): any {
    return http.post('logout');
}

