import http from '@/utils/http';

/**
 * 获取系统用户操作日志列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getLogsList(params?: any) {
    return http.get('system/logs/list', {params});
}

/**
 * 清空所有操作日志
 * @returns {*|Promise<any>}
 */
export function cleanLogs() {
    return http.delete('system/logs/clean');
}

/**
 * 删除操作日志
 * @param params
 * @returns {*|Promise<any>}
 */
export function removeLogs(params: any) {
    return http.delete('system/logs/remove', {params});
}

/**
 * 导出操作日志
 * @param params
 * @returns {*|Promise<any>}
 */
export function exportLogs(params?: any) {
    if (!params) {
        params = {};
    }
    return http.get('system/logs/export', {params});
}
