import http from '@/utils/http';

/**
 * 获取系统用户登录日志列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getLogininforList(params?: any) {
    return http.get('system/logininfor/list', {params});
}

/**
 * 清空所有登录日志
 * @returns {*|Promise<any>}
 */
export function cleanLogininfor() {
    return http.delete('system/logininfor/clean');
}

/**
 * 删除登录日志
 * @param params
 * @returns {*|Promise<any>}
 */
export function removeLogininfor(params: any) {
    return http.delete('system/logininfor/remove', {params});
}

/**
 * 导出登录日志
 * @param params
 * @returns {*|Promise<any>}
 */
export function exportLogininfor(params?: any) {
    if (!params) {
        params = {};
    }
    return http.get('system/logininfor/export', {params});
}
