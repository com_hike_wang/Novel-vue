import http from '@/utils/http';

/**
 * 基本信息
 * @returns {*|Promise<any>}
 */
export function basic() {
    return http.get('/system/druidMonitor/basic');
}

/**
 * 重置所有
 * @returns {*|Promise<any>}
 */
export function resetAll() {
    return http.put('/system/druidMonitor/resetAll');
}

/**
 * 记录并重置所有
 * @returns {*|Promise<any>}
 */
export function logAndReset() {
    return http.put('/system/druidMonitor/logAndReset');
}

/**
 * 数据源信息
 * @returns {*|Promise<any>}
 */
export function datasource() {
    return http.get('/system/druidMonitor/datasource');
}

/**
 * 激活连接的堆栈信息
 * @returns {*|Promise<any>}
 */
export function activeConnectionStackTrace() {
    return http.get('/system/druidMonitor/activeConnectionStackTrace');
}

/**
 * 数据源信息
 * @returns {*|Promise<any>}
 */
export function datasourceById(id) {
    return http.get(`/system/druidMonitor/datasource/${id}`);
}

/**
 * 数据源信息连接信息
 * @returns {*|Promise<any>}
 */
export function connectionInfo(id) {
    return http.get(`/system/druidMonitor/connectionInfo/${id}`);
}

/**
 * 激活连接的堆栈信息
 * @returns {*|Promise<any>}
 */
export function activeConnectionStackTraceById(id) {
    return http.get(`/system/druidMonitor/activeConnectionStackTrace/${id}`);
}

/**
 * sql监控
 * @returns {*|Promise<any>}
 */
export function sql(id) {
    return http.get(`/system/druidMonitor/sql?dataSourceId=${id}`);
}

/**
 * sql Detail
 * @returns {*|Promise<any>}
 */
export function sqlDetail(id) {
    return http.get(`/system/druidMonitor/sqlDetail/${id}`);
}

/**
 * sql监控 wall
 * @returns {*|Promise<any>}
 */
export function wall() {
    return http.get(`/system/druidMonitor/wall`);
}

/**
 * sql监控 wall
 * @returns {*|Promise<any>}
 */
export function wallById(id) {
    return http.get(`/system/druidMonitor/wall/${id}`);
}

/**
 * webUri监控
 * @returns {*|Promise<any>}
 */
export function webUri() {
    return http.get(`/system/druidMonitor/webUri`);
}

/**
 * webUri监控
 * @returns {*|Promise<any>}
 */
export function webUriByUrl(url) {
    return http.get(`/system/druidMonitor/webUriByUrl?url=${url}`);
}

/**
 * webApp监控
 * @returns {*|Promise<any>}
 */
export function webApp() {
    return http.get(`/system/druidMonitor/webApp`);
}

/**
 * spring监控
 * @returns {*|Promise<any>}
 */
export function spring() {
    return http.get(`/system/druidMonitor/spring`);
}

/**
 * spring Detail
 * @returns {*|Promise<any>}
 */
export function springDetail(params) {
    return http.get(`/system/druidMonitor/springDetail`, {params});
}
