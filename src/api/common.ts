import http from '@/utils/http';

/**
 * 获取验证码
 */
export function getKaptchaImage() {
    return http.get('common/captchaImage');
}

/**
 * 获取系统信息
 */
export function getProjectInfo() {
    return http.get('common/getProjectInfo?_' + new Date().getTime());
}
