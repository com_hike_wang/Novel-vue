import http from '@/utils/http';

/**
 * 获取在线用户列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getOnlineList(params?: any) {
    return http.get('monitor/online/list', {params});
}

/**
 * 强制退出用户
 * @param sessionId
 */
export function forceLogout(sessionId: string) {
    return http.delete('monitor/online/' + sessionId);
}
