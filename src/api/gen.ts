import {filterParams} from '@/utils';
import http from '@/utils/http';

/**
 * 获取数据表信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function getGenList(params?: any) {
    if (params) {
        delete params.orderByColumn;
        delete params.isAsc;
    }
    return http.get('tool/gen/list', {params});
}


/**
 * 查询db数据库列表
 * @param params
 * @returns {*|Promise<any>}
 */
export function getGenDBList(params?: any) {
    if (params) {
        delete params.orderByColumn;
        delete params.isAsc;
    }
    return http.get('tool/gen/db/list', {params});
}

/**
 * 导入表
 * @param params
 * @returns {*|Promise<any>}
 */
export function importTable(params?: any) {
    return http.post('tool/gen/importTable', params);
}


/**
 * 删除表数据
 * @param params
 */
export function delTable(params?: any) {
    return http.delete('/tool/gen/' + params.id)
}


/**
 * 同步数据库
 * @param params
 */
export function syncDb(params?: any) {
    return http.get('/tool/gen/syncDb/' + params.tableName)
}


/**
 * 查询表详细信息
 * @param id
 */
export function getGenTable(id: string) {
    return http.get('/tool/gen/' + id)
}


/**
 * 修改代码生成信息
 * @param params
 * @returns {*|Promise<any>}
 */
export function updateGenTable(params?: any) {
    params = filterParams(params);
    params.json = true;

    return http.put('tool/gen', params, {
        headers: {'Content-Type': 'application/json;charset=utf-8'}
    });
}


/**
 * 生成表代码
 * @param params
 * @returns {*|Promise<any>}
 */
export function genCode(params?: any) {
    return http.get('tool/gen/download/' + params.tableName, {responseType: 'blob'});
}


/**
 * 批量生成表代码
 * @param params
 * @returns {*|Promise<any>}
 */
export function batchGenCode(params: any) {
    return http.get('tool/gen/batchGenCode', {params, responseType: 'blob'});
}
