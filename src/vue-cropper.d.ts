import Vue from 'vue';

export declare class VueCropper extends Vue {
    /** Install component into Vue */
    public static install(vue: typeof Vue): void;
}
