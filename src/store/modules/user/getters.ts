import {GetterTree} from 'vuex';

const getters: GetterTree<any, any> = {
    routers: (state) => state.routers,
    addRoutes: (state) => state.addRoutes,
    token: (state) => state.token,
    avatar: (state) => state.avatar,
    name: (state) => state.name,
    userInfo: (state) => state.userInfo,
    permission: (state) => state.permission,
};

export default getters;
