import storeUtils from '@/utils/storeUtils';

const state: any = {
    addRoutes: storeUtils.getStorageS(storeUtils.CONST.addRoutes) || [],
    routers: [],
    permission: storeUtils.getStorageS(storeUtils.CONST.permission) || [],
    userInfo: storeUtils.getStorageS(storeUtils.CONST.userInfo) || {},
    avatar: storeUtils.getStorageS(storeUtils.CONST.avatar) || '',
    name: storeUtils.getStorageS(storeUtils.CONST.name) || 'vue',
    token: storeUtils.getStorageL(storeUtils.CONST.token) || '',
};

export default state;
