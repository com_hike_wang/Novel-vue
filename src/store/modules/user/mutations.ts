import {MutationTree} from 'vuex';
import storeUtils from '@/utils/storeUtils';
import {constantRoutes} from '@/router';

const mutations: MutationTree<any> = {
    SET_ROUTERS: (state: any, routes: any) => {
        state.addRoutes = routes;
        state.routers = constantRoutes.concat(routes);
        storeUtils.setStorageS(storeUtils.CONST.addRoutes, state.addRoutes);
    },
    SET_PERMISSION: (state, permission) => {
        state.permission = permission;
        storeUtils.setStorageS(storeUtils.CONST.permission, state.permission);
    },
    SET_LOGIN_USER_INFO: (state, userInfo) => {
        if (userInfo && userInfo.avatar) {
            userInfo.avatar = process.env.VUE_APP_BASE_API + userInfo.avatar;
        }
        state.userInfo = userInfo;
        storeUtils.setStorageS(storeUtils.CONST.userInfo, state.userInfo);
        if (state.userInfo) {
            state.name = userInfo.name;
            storeUtils.setStorageS(storeUtils.CONST.name, state.name);
            state.avatar = userInfo.avatar;
            storeUtils.setStorageS(storeUtils.CONST.avatar, state.avatar);
        } else {
            state.name = '';
            storeUtils.setStorageS(storeUtils.CONST.name, '');
            state.avatar = '';
            storeUtils.setStorageS(storeUtils.CONST.avatar, '');
        }
    },
    SET_NAME: (state, name) => {
        state.name = name;
        storeUtils.setStorageS(storeUtils.CONST.name, state.name);
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar;
        storeUtils.setStorageS(storeUtils.CONST.avatar, state.avatar);
    },
    SET_TOKEN: (state, token) => {
        state.token = token;
        storeUtils.setStorageL(storeUtils.CONST.token, state.token);
    },
};

export default mutations;
