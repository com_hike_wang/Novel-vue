import storeUtils from '@/utils/storeUtils';
import logo from '@/assets/logo/logo.png';

const state: any = {
    logoSrc: storeUtils.getStorageS(storeUtils.CONST.logoSrc) || logo,
    browserHeaderTitle: 'novel',

    collapse: storeUtils.getStorageS(storeUtils.CONST.collapse) || false,
    projectInfo: storeUtils.getStorageS(storeUtils.CONST.projectInfo) || {
        version: '1.1.0',
        name: 'novel',
        copyrightYear: '2019',
        copyrightCompany: 'cnovel.club',
        default: true,
    },

    visitedViews: [],
    cachedViews: [],
    device: 'desktop',
};

export default state;
