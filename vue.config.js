// 代码压缩
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// gzip压缩
const CompressionWebpackPlugin = require('compression-webpack-plugin');
const path = require("path");
// 是否为生产环境
const isProduction = process.env.NODE_ENV !== 'development';
// 生产环境是否需要使用cdn
const prodNeedCdn = true;
function getVersion(name) {
    const dependencies = require("./package.json").dependencies;
    let version = dependencies[name];
    if (version) {
        version = version.replace("^", "");
    }
    return version;
}
// cdn链接
const cdn = {
    // cdn：模块名称和模块作用域命名（对应window里面挂载的变量名称）
    externals: {
        vue: 'Vue',
        'vuex': 'Vuex',
        'vue-router': 'VueRouter',
        axios: 'axios',
        'element-ui': 'ELEMENT',
        'qs': 'Qs',
        'nprogress': 'NProgress',
        'screenfull': 'screenfull',
        'e-icon-picker': 'eIconPicker',
        'vue-cropper': '["vue-cropper"]',
    },
    // cdn的css链接
    css: [
        `https://unpkg.com/e-icon-picker@${getVersion("e-icon-picker")}/lib/index.css`,
        `https://unpkg.zhimg.com/font-awesome@4.7.0/css/font-awesome.min.css`,
        `https://unpkg.com/element-ui@${getVersion("element-ui")}/lib/theme-chalk/index.css`,
        `https://unpkg.com/nprogress@${getVersion("nprogress")}/nprogress.css`
    ],
    // cdn的js链接
    js: [
        `https://unpkg.com/vue@${getVersion("vue")}/dist/vue.min.js`,
        `https://unpkg.com/vuex@${getVersion("vuex")}/dist/vuex.min.js`,
        `https://unpkg.com/vue-router@${getVersion("vue-router")}/dist/vue-router.min.js`,
        `https://unpkg.com/axios@${getVersion("axios")}/dist/axios.min.js`,
        `https://unpkg.com/element-ui@${getVersion("element-ui")}/lib/index.js`,
        `https://unpkg.com/qs@${getVersion("qs")}/dist/qs.js`,
        `https://unpkg.com/nprogress@${getVersion("nprogress")}/nprogress.js`,
        `https://unpkg.com/screenfull@${getVersion("screenfull")}/dist/screenfull.js`,
        `https://unpkg.com/vue-cropper@${getVersion("vue-cropper")}/dist/index.js`,
        `https://unpkg.com/e-icon-picker@${getVersion("e-icon-picker")}/lib/index.js`,
        `https://unpkg.com/e-icon-picker@${getVersion("e-icon-picker")}/lib/symbol.js`,
    ]
};

module.exports = {
    //vue 中文配置文档地址
    //https://cli.vuejs.org/zh/config/#css-loaderoptions
    // 如果你不需要生产环境的 source map，可以将其设置为 false 以加速生产环境构建。
    productionSourceMap: false,
    chainWebpack: config => {
        config.optimization.minimizer('terser').tap((args) => {
            args[0].terserOptions.compress.drop_console = true;
            return args
        });

        // 移除 prefetch 插件(针对生产环境首屏请求数进行优化)
        config.plugins.delete('prefetch');
        // 移除 preload 插件(针对生产环境首屏请求数进行优化)   preload 插件的用途：https://cli.vuejs.org/zh/guide/html-and-static-assets.html#preload
        config.plugins.delete('preload');
        // ============注入cdn start============
        config.plugin('html').tap(args => {
            // 生产环境或本地需要cdn时，才注入cdn
            if (isProduction && prodNeedCdn) args[0].cdn = cdn;
            return args
        });
        // ============注入cdn start============
    },
    configureWebpack: config => {
        // 生产环境相关配置
        if (isProduction) {
            // 用cdn方式引入，则构建时要忽略相关资源
            if (isProduction && prodNeedCdn) {
                config.externals = cdn.externals;
            }
            // 代码压缩
            config.plugins.push(
                new UglifyJsPlugin({
                    uglifyOptions: {
                        warnings: false,
                        //生产环境自动删除console
                        compress: {
                            drop_debugger: true,
                            drop_console: true,
                        }
                    },
                    sourceMap: false,
                    parallel: true
                })
            );
            // gzip压缩
            const productionGzipExtensions = ['html', 'js', 'css'];
            config.plugins.push(
                new CompressionWebpackPlugin({
                    filename: '[path].gz[query]',
                    algorithm: 'gzip',
                    test: new RegExp(
                        '\\.(' + productionGzipExtensions.join('|') + ')$'
                    ),
                    threshold: 1024, // 只有大小大于该值的资源会被处理 10240
                    minRatio: 0.8, // 只有压缩率小于这个值的资源才会被处理
                    deleteOriginalAssets: false // 删除原文件
                })
            );

            // 公共代码抽离
            config.optimization = {
                splitChunks: {
                    chunks: "all",
                    minSize: 20000, // 允许新拆出 chunk 的最小体积，也是异步 chunk 公共模块的强制拆分体积
                    maxAsyncRequests: 6, // 每个异步加载模块最多能被拆分的数量
                    maxInitialRequests: 6, // 每个入口和它的同步依赖最多能被拆分的数量
                    enforceSizeThreshold: 50000, // 强制执行拆分的体积阈值并忽略其他限制
                    cacheGroups: {
                        vendor: {
                            test: /node_modules/,
                            name: "vendor",
                            priority: 40
                        },
                        common: {
                            chunks: "all",
                            test: path.resolve("src/utils"),
                            name: "common",
                            minChunks: 1,
                            maxInitialRequests: 5,
                            minSize: 0,
                            priority: 100
                        },
                        default: {
                            minChunks: 2, // 覆盖外层minChunks,用于提取被引用指定次数的公共模块，这里默认2次
                            priority: -20,
                            reuseExistingChunk: true // 是否重用已存在的chunk
                        }
                    }
                }
            };
        }
    },
    // 反向代理
    devServer: {
        open: true,
        host: '0.0.0.0',
        port: 8080,
        https: false,
        hotOnly: false,
        proxy: {
            // 配置跨域
            [process.env.VUE_APP_BASE_API]: {
                target: 'http://localhost:8880',
                // target: 'https://cnovel.club/api/',
                ws: true,
                changOrigin: true,
                pathRewrite: {
                    ['^' + process.env.VUE_APP_BASE_API]: ''
                }
            }
        }
    }
};
